package Test2;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RemoveDivisibleByThree {

    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();

        // Add integers to the list
        numbers.add(1);
        numbers.add(3);
        numbers.add(6);
        numbers.add(7);
        numbers.add(9);
        numbers.add(12);
        numbers.add(15);

        System.out.println("Original List: " + numbers);

        // Remove elements divisible by 3
        removeDivisibleByThree(numbers);

        System.out.println("List after removing elements divisible by 3: " + numbers);
    }

    public static void removeDivisibleByThree(List<Integer> numbers) {
        Iterator<Integer> iterator = numbers.iterator();
        while (iterator.hasNext()) {
            int num = iterator.next();
            if (num % 3 == 0) {
                iterator.remove();
            }
        }
    }
}
