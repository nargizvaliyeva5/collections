package Test1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FindMinMaxInList {
    public static void main(String[] args) {
        // Create a list of integers
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(30);
        numbers.add(6);
        numbers.add(15);

        // Find the maximum and minimum values
        int max = findMax(numbers);
        int min = findMin(numbers);

        // Print the results
        System.out.println("List of integers: " + numbers);
        System.out.println("Maximum value: " + max);
        System.out.println("Minimum value: " + min);
    }

    // Method to find the maximum value in the list
    public static int findMax(List<Integer> numbers) {
        if (numbers.isEmpty()) {
            throw new IllegalArgumentException("List is empty. Cannot find maximum.");
        }
        return Collections.max(numbers);
    }

    // Method to find the minimum value in the list
    public static int findMin(List<Integer> numbers) {
        if (numbers.isEmpty()) {
            throw new IllegalArgumentException("List is empty. Cannot find minimum.");
        }
        return Collections.min(numbers);
    }
}
