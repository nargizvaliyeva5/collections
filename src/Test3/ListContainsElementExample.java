package Test3;
import java.util.List;
import java.util.Arrays;

public class ListContainsElementExample {
    public static void main(String[] args) {
        // Sample list of strings
        List<String> listOfStrings = Arrays.asList("apple", "banana", "orange", "grape", "kiwi");

        // Element to check for
        String elementToFind = "orange";

        // Check if the list contains the element
        boolean containsElement = listOfStrings.contains(elementToFind);

        // Output the result
        if (containsElement) {
            System.out.println("The list contains the element: " + elementToFind);
        } else {
            System.out.println("The list does not contain the element: " + elementToFind);
        }
    }
}
