import java.util.HashMap;
import java.util.Map;

public class Inventory {
    private HashMap<String, Integer> inventoryItems;
    public Inventory() {
        this.inventoryItems = new HashMap<String, Integer>();
    }

    public void addItem(String item, int quantity) {
        inventoryItems.put(item, quantity);
    }

    public void updateQuantity(String item, int newQuantity) {
        if(inventoryItems.containsKey(item)) {
            inventoryItems.put(item, newQuantity);
        } else {
            System.out.println("There is no such an item in the inventory");
        }

    }

    public void removeItem(String item) {
        if(inventoryItems.containsKey(item)) {
            inventoryItems.remove(item);
        } else {
            System.out.println("There is no such an item in the inventory");
        }

    }
    public boolean itemExists(String item) {
        return inventoryItems.containsKey(item);
    }
    public void displayInventoryItems() {
        if(inventoryItems.isEmpty()) {
            System.out.println("Inventory is empty");
        }
        for(Map.Entry<String, Integer> item : inventoryItems.entrySet()) {
            System.out.println(item.getKey() + " - " + item.getValue());
        }

    }
}