import java.util.Scanner;

public class InventoryRunner {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        Inventory inventory = new Inventory();

        while(true) {
            System.out.println("1. Add item to the inventory");
            System.out.println("2. Update quantity of an item");
            System.out.println("3. Remove item from inventory");
            System.out.println("4. Check whether an item exists in the inventory or not");
            System.out.println("5. Display inventory items and their quantities");
            System.out.println("6. Exit the program...");

            int option = input.nextInt();
            input.nextLine();

            switch(option) {
                case 1:
                    System.out.println("Enter an item: ");
                    String item = input.nextLine();
                    System.out.println("Enter the quantity: ");
                    int quantity = input.nextInt();
                    inventory.addItem(item, quantity);
                    System.out.println("An item successfully added to the inventory.");
                    break;
                case 2:
                    System.out.println("Enter an item you want to update:");
                    item = input.nextLine();
                    System.out.println("Enter new quantity of item:");
                    quantity = input.nextInt();
                    inventory.updateQuantity(item, quantity);
                    break;
                case 3:
                    System.out.println("Enter an item you want to remove:");
                    item = input.nextLine();
                    inventory.removeItem(item);
                    break;
                case 4:
                    System.out.println("Enter an item you want to check:");
                    item = input.nextLine();
                    if(inventory.itemExists(item)) {
                        System.out.println("Item is already exists in the inventory.");
                    } else {
                        System.out.println("There is no such an item in the inventory.");
                    }
                    break;
                case 5:
                    System.out.println("All Inventory Items");
                    System.out.println("---------------------------");
                    inventory.displayInventoryItems();
                    break;
                case 6:
                    System.out.println("Exiting the program...");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid option, try again...");
                    break;
            }
        }

    }
}
