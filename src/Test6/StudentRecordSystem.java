package Test6;

import java.util.HashMap;

public class StudentRecordSystem {
    private HashMap<Integer, Student> studentRecords;

    public StudentRecordSystem() {
        this.studentRecords = new HashMap<>();
    }

    public void addStudent(Student student) {
        studentRecords.put(student.getId(), student);
    }

    public void removeStudent(int id) {
        studentRecords.remove(id);
    }
    public Student retrieveDetailsByID(int id) {
        return studentRecords.get(id);
    }
    public void printAllStudents() {
        System.out.println("Students:");
        for(Student student : studentRecords.values()) {
            System.out.println("ID: " + student.getId());
            System.out.println("Name: " + student.getName());
            System.out.println("GPA: " + student.getGpa());
            System.out.println("---------------------------------");
        }
    }
}
