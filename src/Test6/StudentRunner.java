package Test6;

import java.util.Scanner;

public class StudentRunner {
    public static void main(String[] args) {

        StudentRecordSystem studentRecordSystem = new StudentRecordSystem();

        Scanner scanner = new Scanner(System.in);

        while(true) {
            System.out.println("1. Add student");
            System.out.println("2. Remove student by id");
            System.out.println("3. Retrieve details by id");
            System.out.println("4. Print all students");
            System.out.println("5. Quit");

            int option = scanner.nextInt();
            scanner.nextLine();

            switch(option) {
                case 1:
                    System.out.println("Enter ID of the student:");
                    int id = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Enter the name of the student:");
                    String name = scanner.nextLine();
                    System.out.println("Enter GPA of the student:");
                    double gpa = scanner.nextDouble();
                    scanner.nextLine();

                    Student student = new Student(id, name, gpa);
                    studentRecordSystem.addStudent(student);
                    break;
                case 2:
                    System.out.println("Enter ID of the student you want to remove:");
                    id = scanner.nextInt();
                    scanner.nextLine();
                    studentRecordSystem.removeStudent(id);
                    break;
                case 3:
                    System.out.println("Enter ID of the student you want to retrieve details:");
                    id = scanner.nextInt();

                    Student retrievedStudent = studentRecordSystem.retrieveDetailsByID(id);
                    if(retrievedStudent!=null) {
                        System.out.println("Retrieved Student Details:");
                        System.out.println("ID: " + retrievedStudent.getId());
                        System.out.println("Name: " + retrievedStudent.getName());
                        System.out.println("GPA: " + retrievedStudent.getGpa());
                    } else {
                        System.out.println("Student was not found.");
                    }
                    break;
                case 4:
                    System.out.println("All students");
                    System.out.println("---------------------------------");
                    studentRecordSystem.printAllStudents();
                    break;
                case 5:
                    System.out.println("Exiting the program...");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid option, try again...");
                    break;

            }
        }

    }
}
