package Test4;
import java.util.ArrayList;
import java.util.List;

public class EvenNumbersAverage {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(2);
        numbers.add(1);
        numbers.add(12);
        numbers.add(10);
        numbers.add(6);

        double average = calculateEvenNumbersAverage(numbers);
        System.out.println("Average of even numbers: " + average);
    }

    public static double calculateEvenNumbersAverage(List<Integer> numbers) {
        int sum = 0;
        int count = 0;

        for (int number : numbers) {
            if (number % 2 == 0) {
                sum += number;
                count++;
            }
        }

        // To avoid division by zero, check if there are even numbers in the list.
        if (count == 0) {
            return 0.0;
        }

        return (double) sum / count;
    }
}
